package com.example.moe.appversion03;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moe.appversion03.Data.DatabaseHandler;
import com.example.moe.appversion03.Data.SharedPref;
import com.example.moe.appversion03.Model.User;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private TextView title;
    private TextView description;
    private TextView price;
    private TextView location;
    private TextView dateAdded;
    private ImageView imageUri;
    private ImageView userImageUri;

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        title = (TextView) findViewById(R.id.titleDet);
        description = (TextView) findViewById(R.id.descriptionDet);
        price = (TextView) findViewById(R.id.priceDet);
        location = (TextView) findViewById(R.id.locationDet);
        dateAdded = (TextView) findViewById(R.id.dateAddedDet);
        imageUri = (ImageView) findViewById(R.id.imageUriDet);
        userImageUri = (ImageView) findViewById(R.id.userImageUri);

        Bundle bundle = getIntent().getExtras();

        DatabaseHandler db = new DatabaseHandler(this);

        title.setText("Pavadinimas: " + bundle.getString("title"));
        description.setText(bundle.getString("description"));
        price.setText(bundle.getString("price"));
        location.setText(bundle.getString("location"));
        dateAdded.setText(bundle.getString("dateAdded"));


        User user = db.getUser(bundle.getInt("postUserId"));

        Picasso.with(getApplicationContext()).load(Uri.parse(user.getImageUri())).into(userImageUri);
        Picasso.with(getApplicationContext()).load(Uri.parse(bundle.getString("imageUri"))).into(imageUri);

        listView = (ExpandableListView)findViewById(R.id.exp);
        initData();
        listAdapter = new com.example.moe.appversion03.UI.ExpandableListAdapter(this, listDataHeader, listHash);
        listView.setAdapter(listAdapter);

    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        DatabaseHandler db = new DatabaseHandler(this);
        User user = db.getUser(bundle.getInt("postUserId"));

        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add(user.getUsername());

        List<String> userData = new ArrayList<>();
        userData.add("User Information ");
        userData.add(user.getFirstName() + "  " + user.getLastName());
        userData.add(user.getEmail());
        userData.add(user.getPhoneNumber());
        userData.add("User Registered On: " + user.getDateItemAdded());



        listHash.put(listDataHeader.get(0),userData);


    }

}
