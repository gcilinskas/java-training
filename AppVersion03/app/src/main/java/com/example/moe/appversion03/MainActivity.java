package com.example.moe.appversion03;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moe.appversion03.Data.DatabaseHandler;
import com.example.moe.appversion03.Data.SharedPref;
import com.example.moe.appversion03.Model.User;
import com.example.moe.appversion03.Util.Constants;

public class MainActivity extends AppCompatActivity {

    private Button registerButton;
    private Button loginButton;
    private EditText loginEmail;
    private EditText loginPassword;
    private DatabaseHandler db;
    private Button install;

    private SharedPref sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        db = new DatabaseHandler(this);

        Log.d("DBPATH", String.valueOf(this.getDatabasePath(Constants.DB_NAME)));

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                // getApplication().deleteDatabase(Constants.DB_NAME);

            }
        });

        loginEmail = (EditText) findViewById(R.id.loginEmail);
        loginPassword = (EditText) findViewById(R.id.loginPassword);

        loginButton = (Button) findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputEmail = loginEmail.getText().toString();
                String inputPassword = loginPassword.getText().toString();
                Boolean checkEmailPass = db.emailpassword(inputEmail, inputPassword);
                if(checkEmailPass==true) {
                    Toast.makeText(getApplicationContext(), "Logged In Succesfully", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(MainActivity.this, com.example.moe.appversion03.HomeActivity.class);
                    intent.putExtra("FirstName", inputEmail);
                    String ID = String.valueOf(db.getUserId(inputEmail));
                    SharedPref.setDefaults("user_id", ID, getApplicationContext());

                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Wrong email or password", Toast.LENGTH_LONG).show();
                }

            }
        });

        install = (Button) findViewById(R.id.install);

        install.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.installCategories();
            }
        });


    }




}
