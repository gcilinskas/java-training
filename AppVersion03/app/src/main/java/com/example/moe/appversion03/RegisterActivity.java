package com.example.moe.appversion03;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.moe.appversion03.Data.DatabaseHandler;
import com.example.moe.appversion03.Data.SharedPref;
import com.example.moe.appversion03.Model.User;

public class RegisterActivity extends AppCompatActivity {

    private EditText firstName;
    private EditText lastName;
    private EditText phoneNumber;
    private EditText email;
    private EditText username;
    private EditText password;
    private Button registerSubmit;
    private DatabaseHandler db;

    private ImageView registerImageUri;
    private Button btnRegisterImageUri;
    private static final int PICK_IMAGE = 100;
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new DatabaseHandler(this);


        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        registerSubmit = (Button) findViewById(R.id.registerSubmit);

        registerImageUri = (ImageView) findViewById(R.id.registerImageUri);
        btnRegisterImageUri = (Button) findViewById(R.id.btnRegisterImageUri);

        btnRegisterImageUri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });


        registerSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!firstName.getText().toString().isEmpty()
                        && !lastName.getText().toString().isEmpty()
                        && !phoneNumber.getText().toString().isEmpty()
                        && !email.getText().toString().isEmpty()
                        && !username.getText().toString().isEmpty()
                        && !password.getText().toString().isEmpty()) {
                    saveUserToDB(v);
                }
            }
        });

    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            registerImageUri.setImageURI(imageUri);
        }
    }

    private void saveUserToDB(View v) {

        User user = new User();

        String newUserFirstName = firstName.getText().toString();
        String newUserLastName = lastName.getText().toString();
        String newUserPhoneNumber = phoneNumber.getText().toString();
        String newUserEmail = email.getText().toString();
        String newUserUsername = username.getText().toString();
        String newUserPassword = password.getText().toString();


        user.setFirstName(newUserFirstName);
        user.setLastName(newUserLastName);
        user.setPhoneNumber(newUserPhoneNumber);
        user.setEmail(newUserEmail);
        user.setUsername(newUserUsername);
        user.setPassword(newUserPassword);
        user.setImageUri(imageUri.toString());

        // save to DB
        db.addUser(user);
        Log.d("User Added: ----", String.valueOf(db.getUsersCount()) + " getUsersCount()");
        Toast.makeText(this,"Item Saved!", Toast.LENGTH_LONG).show();
        Log.d("Vardas : ---- ",user.getFirstName());
        Log.d("UserId : ---- ", String.valueOf(user.getId()) + "-- userId on Register Activity");

        Intent intent = new Intent(RegisterActivity.this, com.example.moe.appversion03.HomeActivity.class);
        intent.putExtra("FirstName", user.getFirstName());
        intent.putExtra("ImageUri", user.getImageUri());

        String ID = String.valueOf(db.getUserId(newUserEmail));
        SharedPref.setDefaults("user_id", ID, getApplicationContext());

        startActivity(intent);

        SharedPref.setDefaults("image_uri", user.getImageUri(), getApplicationContext());
    }
}
