package com.example.moe.appversion03.Data;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.moe.appversion03.Model.Category;
import com.example.moe.appversion03.Model.JobPost;
import com.example.moe.appversion03.Model.Subcategory;
import com.example.moe.appversion03.Model.User;
import com.example.moe.appversion03.Util.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private Context ctx;


    public DatabaseHandler(Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
        this.ctx = context;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }


        String CREATE_USERS_TABLE = "CREATE TABLE " + Constants.TABLE_NAME + "("
                + Constants.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Constants.KEY_FIRST_NAME +
                " TEXT," + Constants.KEY_LAST_NAME + " TEXT," + Constants.KEY_PHONE_NUMBER +
                " TEXT," + Constants.KEY_EMAIL + " TEXT," + Constants.KEY_USERNAME +
                " TEXT," + Constants.KEY_PASSWORD + " TEXT, " + Constants.KEY_IMAGE_URI +
                " TEXT," + Constants.KEY_DATE_NAME + " LONG);";


        String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + Constants.TABLE_NAME_CATEGORIES + "("
                + Constants.KEY_CATEGORIES_ID + " INTEGER PRIMARY KEY," + Constants.KEY_CATEGORIES_NAME + " TEXT);";

        String CREATE_SUBCATEGORIES_TABLE = "CREATE TABLE " + Constants.TABLE_NAME_SUBCATEGORIES + "("
                + Constants.KEY_SUBCATEGORIES_ID + " INTEGER PRIMARY KEY," + Constants.KEY_SUBCATEGORIES_NAME
                + " TEXT," + Constants.KEY_SUBCATEGORIES_CATEGORY_ID + " INTEGER);";


        String CREATE_POSTS_TABLE = "CREATE TABLE " + Constants.TABLE_NAME_POSTS + "("
                + Constants.KEY_POST_ID + " INTEGER PRIMARY KEY," + Constants.KEY_POST_USER_ID + " INTEGER," + Constants.KEY_POST_TITLE
                + " TEXT," + Constants.KEY_POST_DESCRIPTION + " TEXT," + Constants.KEY_POST_PRICE
                + " TEXT," + Constants.KEY_POST_LOCATION + " TEXT," + Constants.KEY_POST_RATING
                + " TEXT," + Constants.KEY_POST_IMAGE_URI + " TEXT," + Constants.KEY_POST_DATE_NAME
                + " LONG," + Constants.KEY_POST_CATEGORY_ID + " INTEGER," + Constants.KEY_POST_SUBCATEGORY_ID + " INTEGER);";

        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_CATEGORIES_TABLE);
        db.execSQL(CREATE_SUBCATEGORIES_TABLE);
        db.execSQL(CREATE_POSTS_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME_POSTS);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME_SUBCATEGORIES);

        onCreate(db);

    }

    /**
     * CRUD OPERATIONS: Create, Read, Update, Delete Methods
     */

    //Add User
    public void addUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_FIRST_NAME, user.getFirstName());
        values.put(Constants.KEY_LAST_NAME, user.getLastName());
        values.put(Constants.KEY_PHONE_NUMBER, user.getPhoneNumber());
        values.put(Constants.KEY_EMAIL, user.getEmail());
        values.put(Constants.KEY_USERNAME, user.getUsername());
        values.put(Constants.KEY_PASSWORD, user.getPassword());
        values.put(Constants.KEY_IMAGE_URI, user.getImageUri());
        values.put(Constants.KEY_DATE_NAME, java.lang.System.currentTimeMillis());

        //Insert the row
        db.insert(Constants.TABLE_NAME, null, values);
        Log.d("DBTEST", "User Saved to DB ---- userId =  " + user.getId());
        Log.d("DBTEST", "User Saved to DB ---- userFirstName =  " + user.getFirstName());
    }



    //Get a User
    public User getUser(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(Constants.TABLE_NAME, new String[] {
                        Constants.KEY_ID, Constants.KEY_FIRST_NAME, Constants.KEY_LAST_NAME,
                        Constants.KEY_PHONE_NUMBER, Constants.KEY_EMAIL, Constants.KEY_USERNAME,
                        Constants.KEY_USERNAME, Constants.KEY_PASSWORD, Constants.KEY_IMAGE_URI,
                        Constants.KEY_DATE_NAME}, Constants.KEY_ID + "=?",
                new String[] {String.valueOf(id)}, null, null, null, null);

        if(cursor != null)
            cursor.moveToFirst();

        User user = new User();
        user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_ID))));
        user.setFirstName(cursor.getString(cursor.getColumnIndex(Constants.KEY_FIRST_NAME)));
        user.setLastName(cursor.getString(cursor.getColumnIndex(Constants.KEY_LAST_NAME)));
        user.setEmail(cursor.getString(cursor.getColumnIndex(Constants.KEY_EMAIL)));
        user.setUsername(cursor.getString(cursor.getColumnIndex(Constants.KEY_USERNAME)));
        user.setPassword(cursor.getString(cursor.getColumnIndex(Constants.KEY_PASSWORD)));
        user.setImageUri(cursor.getString(cursor.getColumnIndex(Constants.KEY_IMAGE_URI)));

        //convert timestamp to something readable
        java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
        String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_DATE_NAME)))
                .getTime());

        user.setDateItemAdded(formatedDate);
        Log.d("DBTEST", "getUser(int) is Called ---- userId =  " + user.getId());
        return user;
    }


    //Get all Users
    public List<User> getAllUsers(){
        SQLiteDatabase db = this.getReadableDatabase();

        List<User> userList = new ArrayList<>();

        Cursor cursor = db.query(Constants.TABLE_NAME, new String[] {
                        Constants.KEY_ID, Constants.KEY_FIRST_NAME, Constants.KEY_LAST_NAME,
                        Constants.KEY_PHONE_NUMBER, Constants.KEY_EMAIL, Constants.KEY_USERNAME,
                        Constants.KEY_USERNAME, Constants.KEY_PASSWORD, Constants.KEY_IMAGE_URI,
                        Constants.KEY_DATE_NAME},
                null, null, null, null,
                Constants.KEY_DATE_NAME + " DESC");

        if(cursor.moveToFirst()) {
            do{
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_ID))));
                user.setFirstName(cursor.getString(cursor.getColumnIndex(Constants.KEY_FIRST_NAME)));
                user.setLastName(cursor.getString(cursor.getColumnIndex(Constants.KEY_LAST_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(Constants.KEY_EMAIL)));
                user.setUsername(cursor.getString(cursor.getColumnIndex(Constants.KEY_USERNAME)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(Constants.KEY_PASSWORD)));
                user.setImageUri(cursor.getString(cursor.getColumnIndex(Constants.KEY_IMAGE_URI)));

                //convert timestamp to something readable
                java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
                String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_DATE_NAME)))
                        .getTime());

                user.setDateItemAdded(formatedDate);

                //Add to UserList
                userList.add(user);
            } while(cursor.moveToNext());
        }

        return userList;
    }




    //Update User
    public int updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_FIRST_NAME, user.getFirstName());
        values.put(Constants.KEY_LAST_NAME, user.getLastName());
        values.put(Constants.KEY_EMAIL, user.getEmail());
        values.put(Constants.KEY_USERNAME, user.getUsername());
        values.put(Constants.KEY_PASSWORD, user.getPassword());
        values.put(Constants.KEY_IMAGE_URI, user.getImageUri());

        //updated row
        return db.update(Constants.TABLE_NAME, values, Constants.KEY_ID + "=?",
                new String[]{ String.valueOf(user.getId()) });

    }

    //Delete User
    public void deleteUser(int id){

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.TABLE_NAME, Constants.KEY_ID + "=?",
                new String[] {String.valueOf(id)});
        db.close();
    }

    //Get count
    public int getUsersCount(){

        String countQuery = "SELECT * FROM " + Constants.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);

        return cursor.getCount();
    }


    /**
     * CRUD OPERATIONS: Create, Read, Update, Delete Methods
     */

    //Add JobPost
    public void addJobPost(JobPost jobPost) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_POST_USER_ID, jobPost.getPostUserId());
        values.put(Constants.KEY_POST_TITLE, jobPost.getTitle());
        values.put(Constants.KEY_POST_DESCRIPTION, jobPost.getDescription());
        values.put(Constants.KEY_POST_PRICE, jobPost.getPrice());
        values.put(Constants.KEY_POST_LOCATION, jobPost.getLocation());
        values.put(Constants.KEY_POST_RATING, jobPost.getRating());
        values.put(Constants.KEY_POST_IMAGE_URI, jobPost.getImageUri());
        values.put(Constants.KEY_POST_DATE_NAME, java.lang.System.currentTimeMillis());
        values.put(Constants.KEY_POST_CATEGORY_ID, jobPost.getCategory_id());
        values.put(Constants.KEY_POST_SUBCATEGORY_ID, jobPost.getSubcategory_id());

        db.insert(Constants.TABLE_NAME_POSTS, null, values);
        Log.d("DBTEST", "Saved Post To DB -- " );

    }


    //Get a JobPost
    public JobPost getJobPost(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(Constants.TABLE_NAME_POSTS, new String[] {
                        Constants.KEY_POST_ID,Constants.KEY_POST_USER_ID, Constants.KEY_POST_TITLE, Constants.KEY_POST_DESCRIPTION, Constants.KEY_POST_PRICE,
                        Constants.KEY_POST_LOCATION, Constants.KEY_POST_RATING, Constants.KEY_POST_IMAGE_URI, Constants.KEY_POST_DATE_NAME, Constants.KEY_POST_CATEGORY_ID,
                        Constants.KEY_POST_SUBCATEGORY_ID},
                Constants.KEY_POST_ID + "=?",
                new String[] {String.valueOf(id)}, null, null, null, null);

        if(cursor != null)
            cursor.moveToFirst();

        JobPost jobPost = new JobPost();
        jobPost.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_ID))));
        jobPost.setPostUserId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_USER_ID))));
        jobPost.setTitle(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_TITLE)));
        jobPost.setDescription(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_DESCRIPTION)));
        jobPost.setPrice(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_PRICE)));
        jobPost.setLocation(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_LOCATION)));
        jobPost.setRating(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_RATING)));
        jobPost.setImageUri(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_IMAGE_URI)));

        jobPost.setCategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_CATEGORY_ID))));
        jobPost.setSubcategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_SUBCATEGORY_ID))));

        //convert timestamp to something readable
        java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
        String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_POST_DATE_NAME)))
                .getTime());

        jobPost.setDateItemAdded(formatedDate);

        return jobPost;
    }

    //Get all Posts
    public List<JobPost> getAllJobPosts(){
        SQLiteDatabase db = this.getReadableDatabase();

        List<JobPost> jobPostList = new ArrayList<>();

        Cursor cursor = db.query(Constants.TABLE_NAME_POSTS, new String[] {
                        Constants.KEY_POST_ID,Constants.KEY_POST_USER_ID, Constants.KEY_POST_TITLE, Constants.KEY_POST_DESCRIPTION, Constants.KEY_POST_PRICE,
                        Constants.KEY_POST_LOCATION, Constants.KEY_POST_RATING, Constants.KEY_POST_IMAGE_URI,
                        Constants.KEY_POST_DATE_NAME, Constants.KEY_POST_CATEGORY_ID, Constants.KEY_POST_SUBCATEGORY_ID},
                null, null, null, null,
                Constants.KEY_POST_DATE_NAME + " DESC");

        if(cursor.moveToFirst()) {
            do{
                JobPost jobPost = new JobPost();
                jobPost.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_ID))));
                jobPost.setPostUserId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_USER_ID))));
                jobPost.setTitle(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_TITLE)));
                jobPost.setDescription(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_DESCRIPTION)));
                jobPost.setPrice(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_PRICE)));
                jobPost.setLocation(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_LOCATION)));
                jobPost.setRating(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_RATING)));
                jobPost.setImageUri(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_IMAGE_URI)));

                jobPost.setCategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_CATEGORY_ID))));
                jobPost.setSubcategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_SUBCATEGORY_ID))));

                //convert timestamp to something readable
                java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
                String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_POST_DATE_NAME)))
                        .getTime());

                jobPost.setDateItemAdded(formatedDate);

                //Add to JobPostList
                jobPostList.add(jobPost);

                Log.d("DBTEST", "Get All Job Posts is Called ---- " + jobPost.getPostUserId());

            } while(cursor.moveToNext());
        }

        return jobPostList;
    }

    //Update JobPost
    public int updateJobPost(JobPost jobPost) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_POST_TITLE, jobPost.getTitle());
        values.put(Constants.KEY_POST_DESCRIPTION, jobPost.getDescription());
        values.put(Constants.KEY_POST_PRICE, jobPost.getPrice());
        values.put(Constants.KEY_POST_LOCATION, jobPost.getLocation());
        values.put(Constants.KEY_POST_RATING, jobPost.getRating());
        values.put(Constants.KEY_POST_IMAGE_URI, jobPost.getImageUri());

        //updated row
        return db.update(Constants.TABLE_NAME_POSTS, values, Constants.KEY_POST_ID + "=?",
                new String[]{ String.valueOf(jobPost.getId()) });

    }

    //Delete JobPost
    public void deleteJobPost(int id){

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.TABLE_NAME_POSTS, Constants.KEY_POST_ID + "=?",
                new String[] {String.valueOf(id)});
        db.close();
    }

    //Get count
    public int getJobPostsCount(){

        String countQuery = "SELECT * FROM " + Constants.TABLE_NAME_POSTS;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);

        return cursor.getCount();
    }


    //Add Category
    public void addCategory(Category category) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_CATEGORIES_NAME, category.getName());

        db.insert(Constants.TABLE_NAME_CATEGORIES, null, values);
        Log.d("DBTEST", "CATEGORY SAVED TO DB ----- categoryName  = " + category.getName());
        Log.d("DBTEST", "CATEGORY SAVED TO DB ----- categoryId  = " + category.getId());
    }


    //Get Category
    public Category getCategory(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(Constants.TABLE_NAME_CATEGORIES, new String[] {
                        Constants.KEY_CATEGORIES_ID, Constants.KEY_CATEGORIES_NAME}, Constants.KEY_CATEGORIES_ID + "=?",
                new String[] {String.valueOf(id)}, null, null, null, null);

        if(cursor != null)
            cursor.moveToFirst();

        Category category = new Category();
        category.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_CATEGORIES_ID))));
        category.setName(cursor.getString(cursor.getColumnIndex(Constants.KEY_CATEGORIES_NAME)));

        return category;

    }


    public List<Category> getAllCategories(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Category> categoryList = new ArrayList<>();

        Cursor cursor = db.query(Constants.TABLE_NAME_CATEGORIES, new String[] {
                        Constants.KEY_CATEGORIES_ID, Constants.KEY_CATEGORIES_NAME},
                null, null, null, null,
                Constants.KEY_CATEGORIES_ID + " DESC");

        if(cursor.moveToFirst()) {
            do{

                Category category = new Category();
                category.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_CATEGORIES_ID))));
                category.setName(cursor.getString(cursor.getColumnIndex(Constants.KEY_CATEGORIES_NAME)));

                categoryList.add(category);
            } while(cursor.moveToNext());
        }

        return categoryList;

    }

    //Get count
    public int getCategoriesCount(){

        String countQuery = "SELECT * FROM " + Constants.TABLE_NAME_CATEGORIES;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);

        return cursor.getCount();
    }







    //Add Subcategory
    public void addSubcategory(Subcategory subcategory) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.KEY_SUBCATEGORIES_NAME, subcategory.getName());
        values.put(Constants.KEY_SUBCATEGORIES_CATEGORY_ID, subcategory.getCategory_id());

        db.insert(Constants.TABLE_NAME_CATEGORIES, null, values);
        Log.d("DBTEST", "SUBCATEGORY SAVED TO DB ----- subcategoryName  = " + subcategory.getName());
    }


    //Get Subcategory
    public Subcategory getSubcategory(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(Constants.TABLE_NAME_SUBCATEGORIES, new String[] {
                        Constants.KEY_SUBCATEGORIES_ID, Constants.KEY_SUBCATEGORIES_NAME, Constants.KEY_SUBCATEGORIES_CATEGORY_ID}, Constants.KEY_SUBCATEGORIES_ID + "=?",
                new String[] {String.valueOf(id)}, null, null, null, null);

        if(cursor != null)
            cursor.moveToFirst();

        Subcategory subcategory = new Subcategory();
        subcategory.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_ID))));
        subcategory.setName(cursor.getString(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_NAME)));
        subcategory.setCategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_CATEGORY_ID))));

        return subcategory;

    }


    public List<Subcategory> getAllSubcategories(){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Subcategory> subcategoryList = new ArrayList<>();

        Cursor cursor = db.query(Constants.TABLE_NAME_SUBCATEGORIES, new String[] {
                        Constants.KEY_SUBCATEGORIES_ID, Constants.KEY_SUBCATEGORIES_NAME, Constants.KEY_SUBCATEGORIES_CATEGORY_ID},
                null, null, null, null,
                Constants.KEY_SUBCATEGORIES_ID + " DESC");

        if(cursor.moveToFirst()) {
            do{

                Subcategory subcategory = new Subcategory();
                subcategory.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_ID))));
                subcategory.setName(cursor.getString(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_NAME)));
                subcategory.setCategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_CATEGORY_ID))));

                subcategoryList.add(subcategory);
            } while(cursor.moveToNext());
        }

        return subcategoryList;

    }

    //Get count
    public int getSubcategoriesCount(){

        String countQuery = "SELECT * FROM " + Constants.TABLE_NAME_SUBCATEGORIES;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);

        return cursor.getCount();
    }


    public Boolean checkEmail(String email){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from " + Constants.TABLE_NAME +
                " where email=?",new String[]{email});


        if(cursor.getCount() > 0) return false;
        else return true;
    }

    public Boolean emailpassword(String email, String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + Constants.TABLE_NAME +
                " where email=? and password=?", new String[] {email,password});
        if(cursor.getCount()>0)
            return true;
        else return false;
    }


    public String getUsername(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "+Constants.KEY_USERNAME+ " FROM " + Constants.TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        String username=cursor.getString(cursor.getColumnIndex(Constants.KEY_USERNAME));
        return username;
    }


    public long getUserId (String email){
        long userId = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {
                Constants.KEY_ID
        };
        String selection = Constants.KEY_EMAIL + " = ?";
        String[] selectionArgs = { email };

        Cursor cursor = db.query(Constants.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.moveToFirst();
        userId = cursor.getLong(cursor.getColumnIndex(Constants.KEY_ID));
        cursor.close();
        db.close();

        if (cursorCount > 0){
            return userId;
        }
        return userId;
    }


    public int getSubcategoryId (String name){
        int subcategoryId = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {
                Constants.KEY_SUBCATEGORIES_ID
        };
        String selection = Constants.KEY_SUBCATEGORIES_NAME + " = ?";
        String[] selectionArgs = { name };

        Cursor cursor = db.query(Constants.TABLE_NAME_SUBCATEGORIES,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.moveToFirst();
        subcategoryId = cursor.getInt(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_ID));
        cursor.close();
        db.close();

        if (cursorCount > 0){
            return subcategoryId;
        }
        return subcategoryId;
    }

    public int getCategoryId (int id){
        int categoryId = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {
                Constants.KEY_SUBCATEGORIES_CATEGORY_ID
        };
        String selection = Constants.KEY_SUBCATEGORIES_ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};

        Cursor cursor = db.query(Constants.TABLE_NAME_SUBCATEGORIES,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null);
        int cursorCount = cursor.getCount();
        cursor.moveToFirst();
        categoryId = cursor.getInt(cursor.getColumnIndex(Constants.KEY_SUBCATEGORIES_CATEGORY_ID));
        cursor.close();
        db.close();

        if (cursorCount > 0){
            return categoryId;
        }
        return categoryId;
    }


//    String[] columns = {
//            Constants.KEY_SUBCATEGORIES_CATEGORY_ID
//    };
//    String selection = Constants.KEY_SUBCATEGORIES_ID + " = ?";
//    String[] selectionArgs = {String.valueOf(id)};
//
//    Cursor cursor = db.query(Constants.TABLE_NAME_SUBCATEGORIES,
//            columns,
//            selection,
//            selectionArgs,



//    public List<JobPost> getSelectedJobPosts(int category_id){
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        String query = "SELECT * FROM " + Constants.TABLE_NAME_POSTS + " WHERE " + Constants.KEY_POST_CATEGORY_ID + " = ?";
//
//        List<JobPost> jobPostList = new ArrayList<>();
//        String selection = Constants.KEY_POST_CATEGORY_ID + " = ?";
//        String[] selectionArgs = {String.valueOf(category_id)};
//
////        Cursor cursor = db.query(Constants.TABLE_NAME_POSTS, new String[] {
////                        Constants.KEY_POST_ID,Constants.KEY_POST_USER_ID, Constants.KEY_POST_TITLE, Constants.KEY_POST_DESCRIPTION, Constants.KEY_POST_PRICE,
////                        Constants.KEY_POST_LOCATION, Constants.KEY_POST_RATING, Constants.KEY_POST_IMAGE_URI,
////                        Constants.KEY_POST_DATE_NAME, Constants.KEY_POST_CATEGORY_ID, Constants.KEY_POST_SUBCATEGORY_ID},
////                selection, selectionArgs, null, null,
////                Constants.KEY_POST_DATE_NAME + " DESC");
//
//        //Cursor cursor = db.rawQuery(query,null);
//        return jobPostList;
//    }



    public List<JobPost> getSelectedJobPosts(int category_id) {

        SQLiteDatabase db = this.getReadableDatabase();
        List<JobPost> jobPostList = new ArrayList<>();

        Cursor cursor = db.rawQuery( "SELECT * FROM " + Constants.TABLE_NAME_POSTS + " WHERE " +
                Constants.KEY_POST_CATEGORY_ID + "=?", new String[] { Integer.toString(category_id) } );




        if(cursor.moveToFirst()) {
            do{
                JobPost jobPost = new JobPost();
                jobPost.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_ID))));
                jobPost.setPostUserId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_USER_ID))));
                jobPost.setTitle(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_TITLE)));
                jobPost.setDescription(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_DESCRIPTION)));
                jobPost.setPrice(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_PRICE)));
                jobPost.setLocation(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_LOCATION)));
                jobPost.setRating(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_RATING)));
                jobPost.setImageUri(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_IMAGE_URI)));

                jobPost.setCategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_CATEGORY_ID))));
                jobPost.setSubcategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_SUBCATEGORY_ID))));

                //convert timestamp to something readable
                java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
                String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_POST_DATE_NAME)))
                        .getTime());

                jobPost.setDateItemAdded(formatedDate);

                //Add to JobPostList
                jobPostList.add(jobPost);

                Log.d("DBTEST", "Get All Job Posts is Called ---- " + jobPost.getPostUserId());

            } while(cursor.moveToNext());
        }

            return jobPostList;
        }




    public List<JobPost> getJoinedPosts(int category_id) {

        SQLiteDatabase db = this.getReadableDatabase();
        List<JobPost> jobPostList = new ArrayList<>();
//
//        Cursor cursor = db.rawQuery( "SELECT * FROM " + Constants.TABLE_NAME_POSTS + " INNER JOIN " + Constants.TABLE_NAME_CATEGORIES + " ON " +
//                 Constants.KEY_POST_CATEGORY_ID + " = " + Constants.KEY_CATEGORIES_ID +
//                " WHERE " + Constants.KEY_POST_CATEGORY_ID + "=?", new String[] { Integer.toString(category_id) } );


        Cursor cursor = db.rawQuery( " select * from postsTBL INNER JOIN categoriesTBL on postsTBL.category_id = categoriesTBL.id WHERE categoriesTBL.id = ?",
                new String[] { Integer.toString(category_id) } );

        if(cursor.moveToFirst()) {
            do{
                JobPost jobPost = new JobPost();
                jobPost.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_ID))));
                jobPost.setPostUserId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_USER_ID))));
                jobPost.setTitle(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_TITLE)));
                jobPost.setDescription(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_DESCRIPTION)));
                jobPost.setPrice(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_PRICE)));
                jobPost.setLocation(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_LOCATION)));
                jobPost.setRating(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_RATING)));
                jobPost.setImageUri(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_IMAGE_URI)));

                jobPost.setCategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_CATEGORY_ID))));
                jobPost.setSubcategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_SUBCATEGORY_ID))));

                //convert timestamp to something readable
                java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
                String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_POST_DATE_NAME)))
                        .getTime());

                jobPost.setDateItemAdded(formatedDate);

                //Add to JobPostList
                jobPostList.add(jobPost);

                Log.d("DBTEST", "Get All Job Posts is Called ---- " + jobPost.getPostUserId());

            } while(cursor.moveToNext());
        }

        return jobPostList;
    }

    public void installCategories(){

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("  INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(1,'PHP',1);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(2,'JAVA',1);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(3,'Mobile Apps',1);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(4,'Logo Design',2);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(5,'Illustration',2);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(6,'Web Design',2);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(7,'Producers',3);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(8,'Voice Over',3);");
                db.execSQL(" INSERT INTO subcategoriesTBL(id,subcategory_name,category_id)VALUES(9,'Mixing',3);");
                db.execSQL(" INSERT INTO categoriesTBL(id,category_name)VALUES(1,'Programming and Tech');");
                db.execSQL(" INSERT INTO categoriesTBL(id,category_name)VALUES(2,'Graphics and Design');");
                db.execSQL(" INSERT INTO categoriesTBL(id,category_name)VALUES(3,'Music and Audio');");
    }

    public List<JobPost> getJoinedSubcategoryPosts(int subcategory_id) {

        SQLiteDatabase db = this.getReadableDatabase();
        List<JobPost> jobPostList = new ArrayList<>();


        Cursor cursor = db.rawQuery( " select * from postsTBL INNER JOIN subcategoriesTBL on postsTBL.subcategory_id = subcategoriesTBL.id WHERE subcategoriesTBL.id = ?",
                new String[] { Integer.toString(subcategory_id) } );

        if(cursor.moveToFirst()) {
            do{
                JobPost jobPost = new JobPost();
                jobPost.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_ID))));
                jobPost.setPostUserId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_USER_ID))));
                jobPost.setTitle(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_TITLE)));
                jobPost.setDescription(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_DESCRIPTION)));
                jobPost.setPrice(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_PRICE)));
                jobPost.setLocation(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_LOCATION)));
                jobPost.setRating(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_RATING)));
                jobPost.setImageUri(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_IMAGE_URI)));

                jobPost.setCategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_CATEGORY_ID))));
                jobPost.setSubcategory_id(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constants.KEY_POST_SUBCATEGORY_ID))));

                //convert timestamp to something readable
                java.text.DateFormat dateFormat = java.text.DateFormat.getDateInstance();
                String formatedDate = dateFormat.format(new Date(cursor.getLong(cursor.getColumnIndex(Constants.KEY_POST_DATE_NAME)))
                        .getTime());

                jobPost.setDateItemAdded(formatedDate);

                //Add to JobPostList
                jobPostList.add(jobPost);

                Log.d("DBTEST", "Get All Job Posts is Called ---- " + jobPost.getPostUserId());

            } while(cursor.moveToNext());
        }

        return jobPostList;
    }






}