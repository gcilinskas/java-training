import java.math.BigDecimal;

public class House {
    String address;
    Double squareMeters;
    Integer numberOfRooms;
    Integer numberOfFloors;
    Character houseNumber;
    Integer numberOfFurniture;
    Integer numberOfWindows;
    Boolean isBuilt;
    BigDecimal pricePerProject;
}
