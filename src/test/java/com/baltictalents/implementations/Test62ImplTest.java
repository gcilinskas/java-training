package com.baltictalents.implementations;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Test62ImplTest {

    static Task62Impl task62;

    @BeforeClass // BeforeClass
    public static void beforeAll() {
        task62 = new Task62Impl();
        System.out.println("Before");
    }

    @AfterClass // AfterAll
    public static void afterAll() {
        task62 = null;
        System.out.println("After");
    }

    @Test
    public void shouldReturnCountOfThreeFullSets() throws Exception {

        List<FigureCount> figures = new ArrayList<>();
        figures.add(new FigureCount(0, 8));
        figures.add(new FigureCount(1, 2));
        figures.add(new FigureCount(2, 2));
        figures.add(new FigureCount(3, 2));
        figures.add(new FigureCount(4, 1));
        figures.add(new FigureCount(5, 1));

        FiguresReader figuresReader = new FiguresReader() {
            @Override
            public List<FigureCount> read() throws Exception {
                return figures;
            }
        };

        assertEquals(1, (long)task62.readAndCalculate(figuresReader));
    }

    @Test
    public void shouldReturnZeroIfNoFigures() throws Exception {

        FiguresReader figuresReader = new FiguresReader() {
            @Override
            public List<FigureCount> read() throws Exception {
                return new ArrayList<>();
            }
        };

        assertEquals(0, (long)task62.readAndCalculate(figuresReader));
    }
}
