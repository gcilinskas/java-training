package com.baltictalents.implementations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class Task1ImplTest {

    @Test
    public void shouldReturnTrueOnPositiveNumbers() {
        assertTrue(Task1Impl.isEvenOfPositive(10));
        assertTrue(Task1Impl.isEvenOfPositive(0));
    }

    @Test
    public void shouldBeFalseOnNegativeNumbers() {
        assertFalse(Task1Impl.isEvenOfPositive(-1));
        assertFalse(Task1Impl.isEvenOfPositive(-10));
    }

    @Test
    public void shouldBeFalseOnPositiveOddNumbers() {
        assertFalse(Task1Impl.isEvenOfPositive(9));
        assertFalse(Task1Impl.isEvenOfPositive(11));
        assertFalse(Task1Impl.isEvenOfPositive(13));
        assertFalse(Task1Impl.isEvenOfPositive(15));
        assertFalse(Task1Impl.isEvenOfPositive(17));
    }


    // toks testas nera naudingas, nes bando paneigti, kad neskaiciuoja trikampio perimetro,
    // tas pats kas paneigti jog negrazina kazkokio skaiciaus is visu Integer.
    @Test
    public void shouldCalculateRectanglePerimeter() {
        assertFalse(Task1Impl.perimeter(3, 4) == 12);
    }

    @Test
    public void shouldNotCalculateOnNegativeNumbers() {
        assertTrue(Task1Impl.perimeter(-1, 12) == -1);
        assertTrue(Task1Impl.perimeter(-1, -12) == -1);
    }

    @Test
    public void shouldNotCalculateIfAtleastOneSideIsZero() {
        assertTrue(Task1Impl.perimeter(0, 0) == 0);
        assertTrue(Task1Impl.perimeter(0, 12) == 0);
        assertTrue(Task1Impl.perimeter(0, -12) == 0);
    }

    @Test
    public void shouldCalculateCorrectPerimeterForRectangle() {
        assertTrue(Task1Impl.perimeter(1, 1) == 4);
        assertTrue(Task1Impl.perimeter(10, 12) == 44);
        assertTrue(Task1Impl.perimeter(3, 3) == 4 * 3);
        assertTrue(Task1Impl.perimeter(4, 4) == 4 * 4);
    }
}
