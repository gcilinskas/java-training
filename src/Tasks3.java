import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Tasks3 {

    // uzpildyti sveikuju skaiciu masyva fibonaci seka. 1000 elementu.
    void task1() {
        Integer n1 = 0;
        Integer n2 = 1;
        Integer n3;
        Integer i;
        Integer j = -1;
        Integer count = 0;

        for( i = 0 ; i > j; ++i ) {
            n3 = n1 + n2;
            System.out.print(count + "\n");
            n1 = n2;
            n2 = n3;
            count += 1;
            if(count == 1001){
                i = 0;
                j = 3;
            }
        }

    }


    // parasyti funkcija kuri nustato ar sveikasis skaicius yra pirminis skaicius.
    void task2() {
        Integer i;
        Boolean isPrime = true;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Select Number to check if number is prime: ");
        Integer n = scanner.nextInt();

        for ( i = 2; i < n; i ++ ) {
            if (n % i == 0) {
                System.out.println("This number is not a prime number!");
                n = 0;
                isPrime = false;
            }
        } if (isPrime) {
            System.out.println("This number is a prime number");
        }

    }


    // rasti pirmus 100 pirminiu skaiciu ir uzpildyti jais masyva
    public static void main(String args[]){
        Integer count = 0;
        Integer checkIfPrime;
        List<Integer> primesArray = new ArrayList<Integer>();

        for (int primeNumber = 2; count < 100; primeNumber++) {
            checkIfPrime = 0;
            for (int divider = 1; divider <= primeNumber; divider++) {

                if (primeNumber % divider == 0) {
                    checkIfPrime++;
                }

            }
            if (checkIfPrime == 2) {
                count++;
                System.out.println(primeNumber + " - This number is prime" + ". Amount of prime numbers in array: " + count);
                primesArray.add(primeNumber);
            }
        }
        System.out.println(Arrays.toString(primesArray.toArray()));
    }


    // aprasyti 3 skirtingas klases kurios tarpusavyje turetu logiska sarysi
    void task4() {
        class House {
            String address;
            Double squareMeters;
            Integer numberOfRooms;
            Integer numberOfFloors;
            Character houseNumber;
            Integer numberOfFurniture;
            Integer numberOfWindows;
            Boolean isBuilt;
            BigDecimal pricePerProject;
        }

        class FirstFloor{
            Double squareMeters;
            Integer numberOfRooms;
            Integer numberOfFurniture;
            Integer numberOfWindows;
            Boolean isBuilt;
            BigDecimal pricePerProject;

        }

        class Kitchen {
            Double squareMeters;
            Integer numberOfFurniture;
            Integer numberOfWindows;
            Boolean isBuilt;
            BigDecimal pricePerProject;
        }
    }

}

