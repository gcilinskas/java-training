package com.baltictalents.test;

import com.baltictalents.implementations.Task1Impl;

public class Task1ImplTest {

    public static void testIsEven() throws Exception {
        assertTrue(Task1Impl.isEvenOfPositive(10));
        assertFalse(Task1Impl.isEvenOfPositive(9));
        assertFalse(Task1Impl.isEvenOfPositive(-1));
        assertFalse(Task1Impl.isEvenOfPositive(-10));
        assertTrue(Task1Impl.isEvenOfPositive(0));
    }

    public static void testPerimeter() throws Exception {
        assertTrue(Task1Impl.perimeter(1, 1) == 4);
        assertTrue(Task1Impl.perimeter(10, 12) == 44);
        assertTrue(Task1Impl.perimeter(0, 0) == 0);
        assertTrue(Task1Impl.perimeter(0, 12) == 0);
        assertTrue(Task1Impl.perimeter(-1, 12) == -1);
        assertTrue(Task1Impl.perimeter(-1, -12) == -1);
        assertFalse(Task1Impl.perimeter(3, 4) == 12);
    }

    static void assertTrue(Boolean t) throws Exception {
        if (!t) throw new Exception("Test failed");
    }

    static void assertFalse(Boolean t) throws Exception {
        if (t) throw new Exception("Test failed");
    }
}
