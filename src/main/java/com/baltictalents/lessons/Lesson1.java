package com.baltictalents.lessons;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Scanner;

public class Lesson1 {

    // Dauguma baziniu duomenu tipu turi savo primityvu tipo atitikmeni, pavyzdziui Byte galima aprasyti kaip byte arba Integer kaip int.
    // Kuo is esmes skiriasi primityvus tipo variantas nuo normalaus paaiskinsiu veliau kai susipazinsime su java objektaisk.
    // Apie primytyvius JAVA tipus siulau savarankiskai pasiskaityti trumpa teksta: https://en.wikibooks.org/wiki/Java_Programming/Primitive_Types
    // Apie operatorius siulau pasiziureti cia, svarbiausi aritmetiniai ir loginiai: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/opsummary.html

    static void textDataTypes() {

        Character c = 'a';
        Character u = '\u039A';

        char cp = 'a';
        char up = '\u039A';

        String s = "hello world";
    }

    static void numericTypes() {

        // sveikieji skaiciai, skiriasi tik reiksmiu dydziu.

        Byte b = 100; // nuo -128 iki 128
        byte bp = 100;


        Short s = 100; // nuo -32768 iki 32767
        short sp = 100;


        Integer i = 100; // -2 billion to +2 billion (approximately)
        int ip = 100;


        Long l = 100L; // -9E18 to +9E18 (approximately)
        long lp = 100;


        // realieji skaiciai. Labiau naudoti atvaizdavimui.

        Float f = 1.0F; // -3.4E38 to +3.4E38
        float fp = 1.0F;

        Double d = 1.0; // -1.7E308 to 1.7E308
        double dp = 1.0;


        // BigDecimal naudoti beveik visada, skaiciavimams, isreiksti pinigu sumas ir pan.

        BigDecimal bigInt = new BigDecimal(1000);
        BigDecimal bigDouble = new BigDecimal(1.0);
     }

     static void booleanType() {

        // loginis tipas, turi tik dvi reiksmes true arba false.

        Boolean b = true;
        boolean bp = true;
     }

     static void dateTypes() {
         LocalDate date = LocalDate.now();
         LocalDateTime dateTime = LocalDateTime.now();
     }


     static void aritmethicOperators() {

         // atimtis, sudetis paprasta operatoriai + ir -

         //division
         Integer a = 5;
         Integer b = 4;

         // operatorius / atlieka paprasta dalyba jeigu dalinami sveikieji skaiciai rezultatas irgi yra tik sveikoji dalis, liekana dingsta
         Integer result = a / b;
         System.out.println("a / b = " + result);

         // operatorius % grazina liekana
         Integer result1 = 100 % 10;
         System.out.println("100 % 10 = " + result1);

         Integer result2 = 1 % 2;
         System.out.println("1 % 2 = " + result2);

         // multiplication
         Integer result3 = 5 * 4;
         System.out.println("5 * 4 = " + result);
     }

     // nebaigta
     static void lessonWork() {
         Scanner scanner = new Scanner(System.in);
         System.out.print("Input age = ");

         Integer age = scanner.nextInt();

         System.out.print("Input hour = ");
         Integer hour = scanner.nextInt();

         System.out.print("Input payment method C - credit card, M - money = ");
         Character paymentMethod = scanner.next().charAt(0);

         Boolean isAgeAllowed = age >= 20;
         Boolean isHourAcceptable = hour >= 10 && hour < 20;

         String result = isAgeAllowed && isHourAcceptable ? "Access" : "Denied";
         System.out.println(result);
     }
}
