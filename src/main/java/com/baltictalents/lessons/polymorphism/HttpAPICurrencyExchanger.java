package com.baltictalents.lessons.polymorphism;

public class HttpAPICurrencyExchanger implements Exchangeable {

    private String host;

    public HttpAPICurrencyExchanger(String host) {
        this.host = host;
    }

    @Override
    public Double convert(String from, String to) {
        return 0.7; // call api by host with param from_to
    }
}
