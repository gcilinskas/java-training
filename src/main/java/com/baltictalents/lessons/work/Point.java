package com.baltictalents.lessons.work;

import java.util.Objects;

public class Point {

    private Double x;
    private Double y;

    public Point() {
        this.x = 0.0;
        this.y = 0.0;
    }

    public Point(String x, String y) {
        this.x = new Double(x);
        this.y = new Double(y);
    }

    public Point(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Point(Double x) {
        this.x = x;
        this.y = 0.0;
    }

    public void move(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        Point point = (Point) o;
        return Objects.equals(x, point.x) &&
                Objects.equals(y, point.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
