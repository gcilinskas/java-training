package com.baltictalents.lessons.work;

public class WrongNumberException extends Exception {

    String number;

    public WrongNumberException(String number) {
        this.number = number;
    }

    @Override
    public String getMessage() {
        return "Wrong number";
    }
}
