package com.baltictalents.lessons.inheritance;

public class Circle extends Figure {

    private Double radius;

    public Circle(Double radius) {
        this.radius = radius;
    }

    @Override
    public Integer edgesCount() {
        return 0;
    }

    @Override
    public Double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public Double perimeter() {
        return 2 * Math.PI * radius;
    }
}
