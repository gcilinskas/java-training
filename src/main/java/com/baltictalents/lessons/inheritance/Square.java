package com.baltictalents.lessons.inheritance;

public class Square extends Figure {

    private Double sideMeters;

    public Square(Double sideMeters) {
        this.sideMeters = sideMeters;
    }

    @Override
    public Integer edgesCount() {
        return 4;
    }

    public Double area() {
        return sideMeters * sideMeters;
    }

    public Double perimeter() {
        return 4 * sideMeters;
    }

    protected Double getSideMeters() {
        return sideMeters;
    }
}
