package com.baltictalents.lessons;

import java.util.*;

public class Lesson6 {

    // antro sprinto pradzia.
    // kolekcijos, equals, comparable, override toString()

    // Kolekcijos
    // List - elementu sarasas, galimos implementacijos ArrayList, LinkedList ir kt.
    // Set  - unikaliu elementu sarasas, galimos implementacijos TreeSet, HashSet ir kt.
    // Map  - elementu rinkinys kurie susideda is rakto ir reiksmes poros, raktai negali dubliuotis, galimos implementacijos HashMap, TreeMap ir kt.

    // equals(), hashCode() - metodai
    void lessonWork() {

        Set<Integer> arrayList = new TreeSet<>();
        List<Integer> linkedList = new LinkedList<>();

        arrayList.add(100);
        arrayList.add(100);
        arrayList.add(200);


        linkedList.add(100);
        linkedList.add(100);
        linkedList.add(100);

        for (Integer i: arrayList) {
            System.out.println(i);
        }

        System.out.println();

        for (Integer i: linkedList) {
            System.out.println(i);
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();


        Map<String, Integer> map = new HashMap<>();
        map.put("Green", 1);
        map.put("Yellow", 1);
        map.put("Red", 1);

        //map.computeIfPresent("Red", (key, value) -> value + 3);

        System.out.println(map.get("Green"));
        System.out.println(map.get("Yellow"));
        System.out.println(map.get("Red"));

        map.keySet();
    }
}
