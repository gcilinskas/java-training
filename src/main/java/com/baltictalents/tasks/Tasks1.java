package com.baltictalents.tasks;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Tasks1 {

    // programa papraso ivesti skaiciu, i ekrana reikia ivesti ar skaicius lyginis ar nelyginis
    void task1() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input digit = ");
        Double task1Number = scanner.nextDouble();
        if ((task1Number % 2) == 0) {
            System.out.print("Entered number is even");
        } else {
            System.out.print("Entered number is odd");
        }
    }
    // programa papraso ivesti skaiciu, i ekrana isvedama skaiciaus saknis
    void task2() {

        Scanner scannerRootNumber = new Scanner(System.in);
        System.out.print("Input digit = ");
        Double task2Number = scannerRootNumber.nextDouble();
        Double task2NumberRoot = Math.sqrt(task2Number);
        if ( task2Number > 0 ) {
            System.out.println(String.format("Square root of number %s = %s", task2Number, task2NumberRoot));
        } else {
            System.out.println("Enter a number greater than 0");
        }

    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedama kvadratines lygties sprendimas x1 = ?, x2 = ?
    void task3() {
        Double x;
        Double x1;
        Double x2;

        Scanner scannerEquationNumber = new Scanner(System.in);
        System.out.print("Enter number a = ");
        Double a = scannerEquationNumber.nextDouble();
        System.out.print("Enter number b = ");
        Double b = scannerEquationNumber.nextDouble();
        System.out.print("Enter number c = ");
        Double c = scannerEquationNumber.nextDouble();

        Double d = b * b - 4 * a * c;
        Double dRoot = Math.sqrt(d);

        if ( d > 0 ) {
            x1 = (-b + dRoot) / 2 * a;
            x2 = (-b - dRoot) / 2 * a;
            System.out.print("x1 = " + x1);
            System.out.print("x2 = " + x2);
        } else if ( d == 0 ) {
            x = -b / 2 * a;
            x1 = x;
            x2 = x;
            System.out.print(String.format("x1 = %s", x1));
            System.out.print(String.format("x2 = %s", x2));
        }else {
            System.out.println("x1 and x2 are imaginary numbers");
        }
    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedam kvadratines lygties reiksmes intervale [-5, 5]
    void task4() {
        Double x;
        Double x1;
        Double x2;

        Integer min = -5;
        Integer max = 5;

        Double a = ThreadLocalRandom.current().nextDouble(min, max + 1);
        Double b = ThreadLocalRandom.current().nextDouble(min, max + 1);
        Double c = ThreadLocalRandom.current().nextDouble(min, max + 1);

        Double d = b * b - 4 * a * c;
        Double dRoot = Math.sqrt(d);

        if ( d > 0 ) {
            x1 = (-b + dRoot) / 2 * a;
            x2 = (-b - dRoot) / 2 * a;
            System.out.print("x1 = " + x1);
            System.out.print("x2 = " + x2);
        } else if ( d == 0 ) {
            x = -b / 2 * a;
            x1 = x;
            x2 = x;
            System.out.print("x1 = " + x1);
            System.out.print("x2 = " + x2);
        } else {
            System.out.println("x1 and x2 are imaginary numbers");
        }

    }


    /*
    XTuple solveSquareEquation(Double a, Double b, Double c) {

        Double bRoot = b * b;
        Double d = bRoot - 4 * a * c;
        XTuple result = new XTuple();
        if (d >= 0) {
            Double dRoot = Math.sqrt(d);
            result.x1 = x(a, b, dRoot);
            result.x2 = d == 0 ? null : x(a, b, -dRoot);
        } else {
            result.x1 = null;
            result.x2 = null;
        }
        return result;
    }

    Double x(Double a, Double b, Double dRoot) {
        return (-b + dRoot) / (2 * a);
    }

    class XTuple {
        Double x1;
        Double x2;
    }


  */
}