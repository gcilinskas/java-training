package com.baltictalents.tasks;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

public class Tasks4 {

    // http://www.egzaminai.lt/failai/7417_IT-VBE-1_2018-GALUTINE.pdf pirma uzduotis
    // kaip ir minejau paskaitos metu, pabandykime isspresti valstybinio egzamino uzduoti

    public static void main(String args[])throws Exception{
        File file = new File("D:/Ui.txt");
        Scanner sc = new Scanner(file);
        Integer n = sc.nextInt();
        Integer sumZ = 0;
        Integer sumR = 0;
        Integer sumG = 0;

        for(Integer j = 1; j <=  n; j++) {

            Character color = sc.next().charAt(0);
            Integer numb = sc.nextInt();
            switch (color){
                case 'Z':
                    sumZ += numb;
                break;
                case 'R':
                    sumR += numb;
                break;
                case 'G':
                    sumG += numb;
                break;
            }

        }

        Integer stripesZ = sumZ / 2;
        Integer stripesR = sumR / 2;
        Integer stripesG = sumG / 2;

        Integer[] stripesArray = {stripesZ, stripesG, stripesR};
        Arrays.sort(stripesArray);
        Integer minN = stripesArray[0];

        Integer leftZ;
        Integer leftR;
        Integer leftG;

        PrintWriter writer = new PrintWriter("D:/UiRes.txt", "UTF-8");
        writer.println(minN);

        if (stripesZ<stripesR && stripesR<stripesG || stripesZ<stripesR && stripesR > stripesG) {
            //System.out.println("The order is: " + stripesZ + " " + stripesR + " " + stripesG  OR  + stripesZ + " " + stripesG + " " + stripesR));

            leftR = sumR - minN * 2;
            leftG = sumG - minN * 2;
            leftZ = sumZ - minN * 2;

            writer.println("G = " + leftG);
            writer.println("Z = " + leftZ);
            writer.println("R = " + leftR);

        }

        else if (stripesR<stripesZ && stripesZ<stripesG || stripesR<stripesZ && stripesZ>stripesG) {
            //System.out.println("The order is: " + stripesR + " " + stripesZ + " " + stripesG  OR  +stripesR+" "+stripesG+" "+stripesZ );

            leftR = sumR - minN * 2;
            leftG = sumG - minN * 2;
            leftZ = sumZ - minN * 2;

            writer.println("G = " + leftG);
            writer.println("Z = " + leftZ);
            writer.println("R = " + leftR);

        }

        else if (stripesG<stripesZ && stripesR<stripesZ || stripesG<stripesZ && stripesR>stripesZ) {
            //System.out.println("The order is: " + stripesG + " " + stripesZ + " " + stripesR  OR  +stripesG+" "+stripesR+" "+stripesZ);

            leftR = sumR - minN * 2;
            leftG = sumG - minN * 2;
            leftZ = sumZ - minN * 2;

            writer.println("G = " + leftG);
            writer.println("Z = " + leftZ);
            writer.println("R = " + leftR);
        }

        writer.close();
    }

    // antra to pacio valstybinio egzamino uzduotis
    void task2() throws Exception {

        File file = new File("D:/Ui2.txt");
        Scanner sc = new Scanner(file);
        Integer n = sc.nextInt();
        String line = sc.nextLine();

        Map<Integer, String> skierNamesStart = new HashMap<>();
        Map<Integer, Integer> skierHoursStart = new HashMap<>();
        Map<Integer, Integer> skierMinutesStart = new HashMap<>();
        Map<Integer, Integer> skierSecondssStart = new HashMap<>();

        Map<Integer, String> skierNamesFinish = new HashMap<>();
        Map<Integer, Integer> skierHoursFinish = new HashMap<>();
        Map<Integer, Integer> skierMinutesFinish = new HashMap<>();
        Map<Integer, Integer> skierSecondssFinish = new HashMap<>();

        Map<Integer, Integer> skierHoursResult = new HashMap<>();
        Map<Integer, Integer> skierMinutesResult = new HashMap<>();
        Map<Integer, Integer> skierSecondsResult = new HashMap<>();

        for( int b = 0; b < n; b++){
            line = sc.nextLine();
            String nameStart = line.substring(0, Math.min(line.length(), 20));
            skierNamesStart.put(b,nameStart);
            line = line.substring(20, line.length());
            String[] sTemp = line.split(" ");
            Integer[] timeStart = new Integer[3];

            Integer sH;
            Integer sM;
            Integer sS;

            for(int i = 0; i < 3; i++) {
                timeStart[i] = Integer.parseInt(sTemp[i]);
                if(timeStart[2] != null){
                    sH = timeStart[0];
                    sM = timeStart[1];
                    sS = timeStart[2];

                    skierHoursStart.put(b, sH);
                    skierMinutesStart.put(b, sM);
                    skierSecondssStart.put(b,sS);
                }
            }
        }

        Integer m = sc.nextInt();
        String lineFinish = sc.nextLine();
        for (int j = 0; j < m ; j++){
            lineFinish = sc.nextLine();
            String nameFinish = lineFinish.substring(0, 20);
            skierNamesFinish.put(j,nameFinish);
            lineFinish = lineFinish.substring(20, lineFinish.length());
            String[] sTemp = lineFinish.split(" ");
            Integer[] timeFinish = new Integer[3];
            Integer sH;
            Integer sM;
            Integer sS;

            for(int i = 0; i < 3; i++) {
                timeFinish[i] = Integer.parseInt(sTemp[i]);
                if(timeFinish[2] != null){
                    sH = timeFinish[0];
                    sM = timeFinish[1];
                    sS = timeFinish[2];

                    skierHoursFinish.put(j, sH);
                    skierMinutesFinish.put(j, sM);
                    skierSecondssFinish.put(j,sS);
                }
            }
        }
        sc.close();

        Skier[] theSkiers = new Skier[m];

        for (Map.Entry<Integer, String> entry : skierNamesStart.entrySet()) {
            for(int l = 0; l < n; l++){
                if (entry.getValue().equals(skierNamesFinish.get(l))) {

                    Integer resultHours = skierHoursFinish.get(l) - skierHoursStart.get(entry.getKey());
                    Integer resultMinutes = skierMinutesFinish.get(l) - skierMinutesStart.get(entry.getKey());
                    Integer resultSeconds = skierSecondssFinish.get(l) - skierSecondssStart.get(entry.getKey());

                    skierHoursResult.put(l, resultHours);
                    if(resultMinutes < 0) {
                        resultMinutes = resultMinutes + 60;
                    }
                    String name = skierNamesFinish.get(l);
                    while (name.length() <= 20) {
                        name = name + " ";
                    }
                    if(resultMinutes < 60) {
                        skierMinutesResult.put(l, resultMinutes);
                        skierSecondsResult.put(l, resultSeconds);
                    }
                    theSkiers[l] = new Skier(name, resultMinutes, resultSeconds);
                }
            }
        }

        List<Skier> skierList = new ArrayList<>(Arrays.asList(theSkiers));

        Comparator<Skier> sortByMinute = new Comparator<Skier>() {
            @Override
            public int compare(Skier o1, Skier o2) {
                return o1.getMinute() - o2.getMinute();
            }
        };

        Comparator<Skier> sortByName = new Comparator<Skier>() {
            @Override
            public int compare(Skier o1, Skier o2) {
                return o2.getName().compareTo(o1.getName());
            }
        };

        Collections.sort(skierList);

        PrintWriter writer = new PrintWriter("D:/Ui2Res.txt", "UTF-8");

        for (Skier i : skierList) {
            writer.println(i);
        }
        writer.close();
    }
}
