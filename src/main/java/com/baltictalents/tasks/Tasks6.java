package com.baltictalents.tasks;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

public class Tasks6 {

    //
    // VBE 2 uzduotis http://www.nec.lt/failai/5256_IT-VBE-1_2015.pdf
    // rekomenduoju susikurti atskira klase siai uzduociai spresti, naudoti java kolekcijas

    void task1()throws Exception {

        File file = new File("D:/egz/2015/Ui2.txt");
        Scanner sc = new Scanner(file);
        Integer n = sc.nextInt();
        Integer m = sc.nextInt();
        Integer target = sc.nextInt();
        String line = sc.nextLine();

        ArrayList<String> sheepNamesList = new ArrayList<>();
        ArrayList<String> sheepDnrStringList = new ArrayList<>();
        ArrayList<Integer> sheepKoefList = new ArrayList<>();

        char[][] dnrChar = new char[target][m];
        char[] targetDnrCharArray = new char[m];
        Integer charIndex = 0;
        String targetName = "";

        for(Integer i = 0; i < n; i++){
            if(i != target-1 ){
                line = sc.nextLine();
                String name = line.substring(0, Math.min(line.length(), 10));
                sheepNamesList.add(name);
                String dnrString = line.substring(11, 11+m);
                sheepDnrStringList.add(dnrString);
                char[] dnrCharArray = dnrString.toCharArray();
                dnrChar[charIndex] = dnrCharArray;
                charIndex++;
            } else {
                line = sc.nextLine();
                targetName = line.substring(0, Math.min(line.length(), 10));
                String targetDnrString = line.substring(11, 11+m);
                targetDnrCharArray = targetDnrString.toCharArray();
            }
        }

        Sheep[] theSheeps = new Sheep[sheepNamesList.size()];
        Integer koef = 0;
        for (int i = 0; i < sheepNamesList.size(); i++){
            for(int j = 0; j < m ; j ++) {
                for(int k = 0; k < m; k++){
                    if(dnrChar[i][j] == targetDnrCharArray[k]){
                        koef++;
                    }
                }
            }
            sheepKoefList.add(koef);
            theSheeps[i] = new Sheep(sheepNamesList.get(i),sheepDnrStringList.get(i), sheepKoefList.get(i));
            koef = 0;
        }

        List<Sheep> sheepList = new ArrayList<>(Arrays.asList(theSheeps));

        Comparator<Sheep> sortByKoef = new Comparator<Sheep>() {
            @Override
            public int compare(Sheep o1, Sheep o2) {
                return o1.getKoef() - o2.getKoef();
            }
        };

        Comparator<Sheep> sortByName = new Comparator<Sheep>() {
            @Override
            public int compare(Sheep o1, Sheep o2) {
                return o2.getName().compareTo(o1.getName());
            }
        };

        Collections.sort(sheepList);
        PrintWriter writer = new PrintWriter("D:/egz/2015/Ui2Res.txt", "UTF-8");
        writer.println(targetName);
        for (Sheep i : sheepList) {
            writer.println(i);
        }
        writer.close();

    }

    // VBE 2 uzduotis http://www.nec.lt/failai/1602_IT-pagr-2010.pdf
    // rekomendacijos tos pacios kaip ir task1
    public static void main(String args[]) throws Exception {

        File file = new File("D:/egz/2010/Ui2.txt");
        Scanner sc = new Scanner(file);
        Integer n = sc.nextInt();
        Integer m = sc.nextInt();
        Integer[] productPriceArray = new Integer[n];
        for(int i = 0; i < n; i++){
            Integer p = sc.nextInt();
            Integer price = p;
            productPriceArray[i] = p;
        }
        String[] dishTitle = new String[m];
        Integer[][] productQ = new Integer[m][n];
        String line = sc.nextLine();
        for(int i = 0; i < m; i++){
            line = sc.nextLine();
            String name = line.substring(0, 15);
            dishTitle[i] = name;
            String quantity = line.substring(16, line.length());
            String[] sTemp = quantity.split(" ");
            Integer[] quantityArray = new Integer[n];
            for (int j = 0; j < n; j++){
                quantityArray[j] = Integer.parseInt(sTemp[j]);
            }
            productQ[i] = quantityArray;
        }

        Integer[][] dishPriceArray = new Integer[m][n];
        Integer[] dishPrice = new Integer[m];
        Integer sum = 0;
        Integer sumTotal = 0;

        for(int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                dishPriceArray[i][j] = productQ[i][j] * productPriceArray[j];
                sum = sum + dishPriceArray[i][j];
            }
            dishPrice[i] = sum;
            sumTotal = sumTotal + dishPrice[i];
            sum = 0;
        }

        double sumTotalLtDouble = Math.floor(sumTotal/100.0);
        Integer sumTotalLt = (int)sumTotalLtDouble;
        Integer sumTotalCt = sumTotal - sumTotalLt*100;

        PrintWriter writer = new PrintWriter("D:/egz/2010/Ui2Res.txt", "UTF-8");

        for(int i = 0; i < m; i++){
            writer.println(dishTitle[i] + " " + dishPrice[i]);
        }
        writer.println(sumTotalLt + " " + sumTotalCt);
        writer.close();

    }
}
