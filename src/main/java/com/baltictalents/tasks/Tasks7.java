package com.baltictalents.tasks;


import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Tasks7 {

    // testuojantis naudoti POSTMAN

    // sukurti http servisa, kuris turetu du endpointus:
    // 1. GET http://localhost:8080/currencies
    // 2. GET http://localhost:8080/currency/rates?currencyCode=USD
    // responsai turi buti grazinami json formatu.
    //  pvz: ["USD", "EUR", "GBP"]
    //
    public static void main(String[] args) throws Exception {


        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/currencies", new MyHandler());
        server.createContext("/currency/rates", new MyHandler());
        server.setExecutor(null);
        server.start();
    }

        static class MyHandler implements HttpHandler {
            @Override
            public void handle(HttpExchange t) throws IOException {
                String response = "[\"USD\", \"EUR\", \"GBP\"]";
                t.sendResponseHeaders(200, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            }
        }

}