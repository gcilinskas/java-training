package com.baltictalents.implementations;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task62Impl {

    public final static int UNIQUE_FIGURES = 6;
    public final static int[] FIGURES_FREQUENCES = {8, 2, 2, 2, 1, 1};

    private Integer calculate(List<FigureCount> figures) throws Exception {

        int[] sumsOfUniqueFigures = new int[UNIQUE_FIGURES];

        for (FigureCount figure: figures) {
            sumsOfUniqueFigures[figure.type] += figure.count;
        }

        int setsCount = sumsOfUniqueFigures[0] / FIGURES_FREQUENCES[0];
        for (int i = 1; i < UNIQUE_FIGURES; i++) {
            int setsByFigure = sumsOfUniqueFigures[i] / FIGURES_FREQUENCES[i];
            if (setsCount > setsByFigure) {
                setsCount = setsByFigure;
            }
        }
        return setsCount;
    }

    private List<FigureCount> readFigures() throws Exception {
        File file = new File("resources/Task6Input.txt");
        Scanner in = new Scanner(file);

        Integer n = in.nextInt();

        List<FigureCount> figures = new ArrayList<>(Task62Impl.UNIQUE_FIGURES * n);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < Task62Impl.UNIQUE_FIGURES; j++) {
                int figureCount = in.nextInt();
                if (figureCount > 0) {
                    FigureCount figure = new FigureCount(j, figureCount);
                    figures.add(figure);
                }
            }
        }
        return figures;
    }


    static void fillByStream() {

        List<FigureCount> figureCounts = Stream.of(1, 2, 3, 4, 5, 6)
                .map(i -> new FigureCount(i - 1, i * 10))
                .collect(Collectors.toList());
    }

    public Integer readAndCalculate(FiguresReader figuresReader) throws Exception {
        return calculate(figuresReader.read());
    }
}
