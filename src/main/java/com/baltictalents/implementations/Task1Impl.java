package com.baltictalents.implementations;

public class Task1Impl {

    public static boolean isEvenOfPositive(Integer number) {
        return number >= 0 && number % 2 == 0;
    }


    // when a or b is 0 then returns 0
    // when a or b is negative returns -1
    // when a and b is positive returns calculated perimeter
    public static Integer perimeter(Integer a, Integer b) {
        if (a == 0 || b == 0) {
            return 0;
        }
        return a < 0 || b < 0 ? -1 : 2 * a + 2 * b;
    }
}
