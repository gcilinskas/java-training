

import java.math.BigDecimal;

/*
    Zmogus(
            String name,
            String lastname,
            String personalCode,
            Float weightKg,
            Short heightCm,
            Boolean hasChildren,
            LocalDate bornDate,
            Character sex,
            String bloodType
    ) {
        this.name = name;
        this.lastname = lastname;
        this.personalCode = personalCode;
        this.weightKg = weightKg;
        this.heightCm = heightCm;
        this.hasChildren = hasChildren;
        this.bornDate = bornDate;
        this.sex = sex;
        this.bloodType = bloodType;
    }

||||||| merged common ancestors
import java.math.RoundingMode;
import java.time.LocalDate;
=======
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
>>>>>>> 0763f9455684bcbec6fccc219e44d9005380d377

    Double kmi() {
        Double heightM = heightCm / 100.0;
        System.out.println(heightM);

        Double kmi = weightKg / (heightM * heightM);
        BigDecimal bd = new BigDecimal(kmi);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    static String[] defaultBloodTypes() {
        return defaultBloodTypes;
    }

    private final static Double cmInM = 100.0;

    Zmogus(
            String name,
            String lastname,
            String personalCode,
            Float weightKg,
            Short heightCm,
            Boolean hasChildren,
            LocalDate bornDate,
            Character sex,
            String bloodType
    ) {
        this.name = name;
        this.lastname = lastname;
        this.personalCode = personalCode;
        this.weightKg = weightKg;
        this.heightCm = heightCm;
        this.hasChildren = hasChildren;
        this.bornDate = bornDate;
        this.sex = sex;
        this.bloodType = bloodType;
    }

    Double kmi() {
        Double heightM = heightCm / cmInM;
        System.out.println(heightM);

        Double kmi = weightKg / (heightM * heightM);
        BigDecimal bd = new BigDecimal(kmi);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    static String[] defaultBloodTypes() {
        return defaultBloodTypes;
    }

*/

/*
public class Zmogus {
    String name;
    Float weight;
    Float height;
    String lastname;
    Boolean education;
    BigDecimal salary;
    Character firstNameLetter;

    String initials() {
        return name.substring(0, 1) + lastname.substring(0, 1);
    }

    String initials() {
        return name.substring(0, 1) + lastname.substring(0, 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zmogus zmogus = (Zmogus) o;
        return Objects.equals(name, zmogus.name) &&
                Objects.equals(lastname, zmogus.lastname) &&
                Objects.equals(personalCode, zmogus.personalCode) &&
                Objects.equals(weightKg, zmogus.weightKg) &&
                Objects.equals(heightCm, zmogus.heightCm) &&
                Objects.equals(hasChildren, zmogus.hasChildren) &&
                Objects.equals(bornDate, zmogus.bornDate) &&
                Objects.equals(sex, zmogus.sex) &&
                Objects.equals(bloodType, zmogus.bloodType) &&
                Arrays.equals(transactions, zmogus.transactions);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(name, lastname, personalCode, weightKg, heightCm, hasChildren, bornDate, sex, bloodType);
        result = 31 * result + Arrays.hashCode(transactions);
        return result;
    }

}

*/