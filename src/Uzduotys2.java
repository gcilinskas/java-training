public class Uzduotys2 {

    /*
    Duoti trys skaičiai: a, b, c. Nustatykite ar šie skaičiai gali būti
    trikampio kraštinių ilgiai ir jei gali tai kokio trikampio:
    lygiakraščio, lygiašonio ar įvairiakraščio. Atspausdinkite
    atsakymą. Kaip pradinius duomenis panaudokite tokius skaičius:
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */

    public static void task1() {
        Integer[][] trianglesArray = new Integer[4][3];
        trianglesArray[0][0] = 3;
        trianglesArray[0][1] = 4;
        trianglesArray[0][2] = 5;

        trianglesArray[1][0] = 2;
        trianglesArray[1][1] = 10;
        trianglesArray[1][2] = 8;

        trianglesArray[2][0] = 5;
        trianglesArray[2][1] = 6;
        trianglesArray[2][2] = 5;

        trianglesArray[3][0] = 5;
        trianglesArray[3][1] = 5;
        trianglesArray[3][2] = 5;

        for (Integer b = 0; b < trianglesArray.length; b++) {

            for (Integer j = 0; j < trianglesArray[b].length - 1; j++) {

                if (trianglesArray[b][j] == trianglesArray[b][j+1]) {
                    System.out.println(String.format("Trikampis su krastinėmis %s, %s, %s yra lygiašonis", trianglesArray[b][0], trianglesArray[b][1], trianglesArray[b][2]) );
                } else if (trianglesArray[b][0] == trianglesArray[b][1] || trianglesArray[b][0] == trianglesArray[b][2] || (trianglesArray[b][1] == trianglesArray[b][2]) ) {
                    System.out.println(String.format("Trikampis su krastinėmis %s, %s, %s yra Lygiakrastis", trianglesArray[b][0], trianglesArray[b][1], trianglesArray[b][2]) );
                } else if(trianglesArray[b][j] != trianglesArray[b][j+1]) {
                    System.out.println(String.format("Trikampis su krastinėmis %s, %s, %s yra įvairiakraštis", trianglesArray[b][0], trianglesArray[b][1], trianglesArray[b][2]) );
                }
                j += 1;

            }

        }

    }


    /*
    Apskaiciuoti trikampiu plotus
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
    public static void task2() {
        Integer[][] trianglesArray = new Integer[4][3];
        trianglesArray[0][0] = 3;
        trianglesArray[0][1] = 4;
        trianglesArray[0][2] = 5;

        trianglesArray[1][0] = 2;
        trianglesArray[1][1] = 10;
        trianglesArray[1][2] = 8;

        trianglesArray[2][0] = 5;
        trianglesArray[2][1] = 6;
        trianglesArray[2][2] = 5;

        trianglesArray[3][0] = 5;
        trianglesArray[3][1] = 5;
        trianglesArray[3][2] = 5;

        for (Integer b = 0; b < trianglesArray.length; b++) {

            for (Integer j = 0; j < trianglesArray[b].length - 1; j++) {

                Integer firstSide = trianglesArray[b][0];
                Integer secondSide = trianglesArray[b][1];
                Integer thirdSide = trianglesArray[b][2];

                if (trianglesArray[b][j] == trianglesArray[b][j+1]) {
                    Double p = ( firstSide + secondSide + thirdSide ) / 2.0;
                    Double s = p * ( p - firstSide ) * ( p - secondSide ) * ( p - thirdSide) ;
                    Double answer = Math.sqrt(s);
                    System.out.println(String.format("Trikampis yra lygiasonis ir jo plotas lygus %s, kurio kraštinės yra %s, %s, %s", answer , firstSide, secondSide, thirdSide) );
                } else if (firstSide == secondSide || firstSide == thirdSide || (secondSide == thirdSide) ) {
                    Double answer = ( firstSide * firstSide * Math.sqrt(3) ) / 4;
                    System.out.println(String.format("Trikampis yra lygiakrastis ir jo plotas lygus %s, kurio kraštinės yra %s, %s, %s", answer , firstSide, secondSide, thirdSide) );
                } else if(trianglesArray[b][j] != trianglesArray[b][j+1]) {
                    Double p = ( firstSide + secondSide + thirdSide ) / 2.0;
                    Double s = p * ( p - firstSide ) * ( p - secondSide ) * ( p - thirdSide );
                    Double answer = Math.sqrt(s);
                    System.out.println(String.format("Trikampis yra įvairiakraštis ir jo plotas lygus %s, kurio kraštinės yra %s, %s, %s", answer , firstSide, secondSide, thirdSide) );
                }
                j += 1 ;

            }

        }

    }


    // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
    // surasti min ir max
    // negalima naudoti min, max;
    public static void main(String[] params) {
        Integer[] integers = {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15};
        Integer maxValue = integers[0];

        for(int i=1;i < integers.length;i++){
            if(integers[i] > maxValue){
                maxValue = integers[i];
            }
        }
        System.out.println("Max value: " + maxValue);


        Integer minValue = integers[0];
        for(int i=1;i<integers.length;i++){
            if(integers[i] < minValue){
                minValue = integers[i];
            }
        }
        System.out.println("Min value: " + minValue);
    }


    // task 4
    // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
    // isrikiuoti didejimo tvarka
    // negalima naudoti Collections.sort(array[Int]);
    void task4() {

        Integer numbersArray[]= {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15};
        Integer numbersInOrder[]=new Integer[10];
        Integer greater;

        for(int indexL=0;indexL<numbersArray.length;indexL++) {
            greater=0;
            for(int indexR=0;indexR<numbersArray.length;indexR++) {
                if(numbersArray[indexL]>numbersArray[indexR]) {
                    greater++;
                }
            }
            while (numbersInOrder[greater] == numbersArray[indexL]) {
                greater++;
            }
            numbersInOrder[greater] = numbersArray[indexL];
        }

        for(greater=0;greater<numbersInOrder.length;greater++) {
            System.out.print(numbersInOrder[greater]+" ");
        }

    }

}
