package com.baltictalents.tasks;

public class Skier implements Comparable<Skier> {

    private final String name;
    private final Integer minute;
    private final Integer second;

    public Skier(String name, Integer minute, Integer second) {
        this.name = name;
        this.minute = minute;
        this.second = second;
    }

    public String getName() {
        return name;
    }


    public Integer getMinute() {
        return minute;
    }

    public Integer getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return name + minute + " " + second;
    }

    @Override
    public int compareTo(Skier o) {

        if (o.getMinute() == minute) {
            return name.compareTo(o.getName());
        } else {
            return  minute - o.getMinute();
        }

    }
}
