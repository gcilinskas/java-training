package com.baltictalents.tasks.Task7_2;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
//
public class App {

    public static void main(String args[]) throws Exception {

    String fromCurrencyUSD = "USD";
    String toCurrencyEUR = "EUR";
    String toCurrencyGBP = "GBP";
    URL url2 = new URL ("http://free.currencyconverterapi.com/api/v5/convert?q=" + fromCurrencyUSD + "_" + toCurrencyEUR + "&compact=y");

    ExecutorService executor = Executors.newFixedThreadPool(1);
    Future<Response> response = executor.submit(new Request(url2));
    InputStream body = response.get().getBody();

    String result = new BufferedReader(new InputStreamReader(body)).lines().collect(Collectors.joining("\n"));
    System.out.println(result);

    executor.shutdown();
    }

}
