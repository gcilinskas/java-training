package com.baltictalents.tasks;
//
public class Sheep implements Comparable<Sheep> {

    private final String name;
    private final String dnrString;
    private final Integer koef;

    public Sheep(String name, String dnrString, Integer koef) {
        this.name = name;
        this.dnrString = dnrString;
        this.koef = koef;
    }

    public String getName() {
        return name;
    }

    public String getDnrString() {
        return dnrString;
    }

    public Integer getKoef() {
        return koef;
    }

    @Override
    public String toString() {
        return name + " " + koef;
    }

    @Override
    public int compareTo(Sheep o) {

        if (o.getKoef() == koef) {
            return name.compareTo(o.getName());
        } else {
            return   o.getKoef() - koef;
        }

    }
}
