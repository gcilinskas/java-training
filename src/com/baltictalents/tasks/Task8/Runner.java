package com.baltictalents.tasks.Task8;

import java.util.*;

public class Runner extends Thread {

    private int id;

    public Runner(int id){
        this.id = id;
    }
    public List<Integer> randomList = new ArrayList<>();
    public Map<Integer, Integer> randomMap = new HashMap<>();

    public void run() {

        for (int i = 0; i < 100; i++){
            Random random = new Random();
            Integer result = random.nextInt(100-1) + 1;
            randomList.add(result);
            randomMap.put(id, result);

        }
        Collections.sort(randomList);
    }






}
