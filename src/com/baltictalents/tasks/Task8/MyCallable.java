package com.baltictalents.tasks.Task8;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class MyCallable implements Callable<Integer> {

    public Integer call() throws Exception {
        Thread.sleep(1000);
        //return the thread name executing this callable task
        return Integer.valueOf(Thread.currentThread().getName());
    }

    public static void main(String[] args){

        Integer n = 5;

        ExecutorService executor = Executors.newFixedThreadPool(n);

        List<Future<Integer>> list = new ArrayList<Future<Integer>>();

        Callable<Integer> callable = new MyCallable();

        for(int i=0; i< 100; i++){
            //submit Callable tasks to be executed by thread pool
            Future<Integer> future = executor.submit(callable);
            //add Future to the list, we can get return value using Future
            list.add(future);
        }

        for(Future<Integer> fut : list){
            try {
                //print the return value of Future, notice the output delay in console
                // because Future.get() waits for task to get completed
                System.out.println("::"+fut.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    }




}
