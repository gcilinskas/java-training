package com.baltictalents.tasks.Task8;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Task8{

    //    Sukurkite programą kur keletas objektų (tarkime kokie 5) vienu
//    metu sugeneruoja po šimtą atsitiktinių skaičių iš intervalo
//    1..100 (imtinai). Išveskite tuos skaičius didėjimo tvarka kartu su
//    pasikartojimo skaičiumi, pvz.: 1 (5), 3 (1), ... - tai reiškia, kad
//    skaičius 1 buvo sugeneruotas 5 kartus, skaičius 2 - visai
//    nebuvo, 3 - vieną kartą ir t.t.


    public static void main(String[] args) {

        Integer n = 5;

        ExecutorService executor = Executors.newFixedThreadPool(n);


        List<Future<Integer>> futureList = new ArrayList<>();

        for(int i = 0; i < n; i++){
          executor.submit(new Runner(i));
        }
    }
}







/*        // multithreading

        List<Integer> list = new ArrayList<>();

        void lessonWork() {
            Executor executor = Executors.newFixedThreadPool(5);

            Task8 t1 = new Task8();

            for (int i = 1; i < 5; i++) {
                executor.execute(() -> {
                    IntStream.range(1, 101)
                            .forEach(Task8::printer);
                });
            }
        }



        void printer(Integer i) {
            synchronized (list) {
                if (!list.contains(i)) {
                    list.add(i);
                }
            }

            System.out.println(i);
        }


    }*/




/* // how to aggregate results from async task/futures
        void futures() throws Exception {
            ExecutorService executor = Executors.newFixedThreadPool(5);

            Main main = new Main();

            Long start = System.currentTimeMillis();

            Future<Integer> futureA = executor.submit(() -> {
                Thread.sleep(1000);
                return 100;
            });

            Future<Integer> futureB = executor.submit(() -> {
                Thread.sleep(500);
                return 200;
            });

            System.out.println(futureA.get() + futureB.get());
            System.out.println("Time: " + (System.currentTimeMillis() - start));
        }*/

