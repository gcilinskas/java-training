package com.baltictalents.lessons.inheritance;

public class Hexagon extends Figure {

    private Double sideMeters;

    public Hexagon(Double sideMeters) {
        this.sideMeters = sideMeters;
    }

    @Override
    protected Integer edgesCount() {
        return 6;
    }

    public Double area() {
        return ( ( 3 * Math.sqrt(3) ) / 2 ) * sideMeters * sideMeters;
    }

    public Double perimeter() {
        return 6 * sideMeters;
    }

}