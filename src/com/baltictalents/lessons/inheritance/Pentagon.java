package com.baltictalents.lessons.inheritance;

public class Pentagon extends Figure {

    private Double sideMeters;

    public Pentagon(Double sideMeters) {
        this.sideMeters = sideMeters;
    }

    @Override
    protected Integer edgesCount() {
        return 5;
    }

    public Double area() {
        return ( ( Math.sqrt(25 + 10 * Math.sqrt(5)) ) / 2 ) * sideMeters * sideMeters;
    }

    public Double perimeter() {
        return 5 * sideMeters;
    }

}
